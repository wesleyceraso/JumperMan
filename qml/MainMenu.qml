import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import "panels"
import "widgets"
import "popups"

Image {
    id: mainmenu
    anchors.fill: parent

    signal menu
    signal play

    source: "qrc:///background_level1.png"
    fillMode: Image.Stretch

    LifesPanel {
        id: lifesDisplay
        height: mainmenu.height * 0.08
        width: height * 2.0
        z: 2
        lifes: account.lifes
        showTimeToGetLife: account.lifes < account.maximumLifes

        anchors {
            top: mainmenu.top
            left: mainmenu.left
            margins: mainmenu.height * 0.01
        }
    }

    Column {
        id: column
        anchors {
            bottom: mainmenu.bottom
            right: mainmenu.right
            margins: mainmenu.width * 0.02
        }
        spacing: mainmenu.height * 0.02
        enabled: exitConfirmationPopUp.state == "notVisible"

        CustomButton {
            id: unlockButton
            height: mainmenu.height * 0.15
            width: 4 * height
            text: "Unlock Levels"
            upperColor: Qt.lighter(bottomColor)
            bottomColor: "#E9E0C5"
            anchors.right: parent.right
            enabled: column.enabled
        }


        CustomButton {
            id: connectFacebookButton
            text: "Connect"
            upperColor: Qt.lighter(bottomColor)
            bottomColor: "#E9E0C5"

            style: ButtonStyle {
                background: TwoColoredPanel {
                    upperColor: !control.pressed ? control.upperColor : Qt.darker(control.upperColor)
                    bottomColor: !control.pressed ? control.bottomColor : Qt.darker(control.bottomColor)
                }
                label: Item {
                    anchors.fill: parent
                    Row {
                        anchors.centerIn: parent
                        spacing: 10
                        Text {
                            id: faceText
                            text: control.text
                            font.pixelSize: connectFacebookButton.height * 0.6
                            anchors.verticalCenter: parent.verticalCenter
                        }
                        FacebookIcon {
                            anchors.verticalCenter: parent.verticalCenter
                            font.pixelSize: connectFacebookButton.height * 0.6
                        }
                    }
                }
            }

            height: mainmenu.height * 0.15
            width: 4 * height
            anchors.right: parent.right
            enabled: column.enabled
        }

        CustomButton {
            id: playButton
            height: mainmenu.height * 0.20
            width: 4 * height
            text: "Play"
            upperColor: "#FFD280"
            bottomColor: "orange"
            anchors.right: parent.right

            onClicked: {
                if (account.lifes > 0) {
                    mainmenu.play()
                }
                else {
                    storePopUp.visible = true
                }
            }
            enabled: column.enabled
        }
    }

    focus: true
    Keys.onBackPressed: {
        exitConfirmationPopUp.state = "visible";
    }

    ExitConfirmationPopUp {
        id: exitConfirmationPopUp
        anchors.horizontalCenter: mainmenu.horizontalCenter
        z: 1
        width: mainmenu.width * 0.70
        height: mainmenu.height * 0.30

        onResume: exitConfirmationPopUp.state = "notVisible";
        onExit: Qt.quit()

        state: "notVisible"
        states: [
            State {
                name: "visible"
                AnchorChanges {target: exitConfirmationPopUp; anchors.verticalCenter: mainmenu.verticalCenter}
            },
            State {
                name: "notVisible"
                AnchorChanges {target: exitConfirmationPopUp; anchors.top: mainmenu.bottom}
            }
        ]

        transitions: [
            Transition {
                from: "notVisible"
                to: "visible"
                reversible: true
                AnchorAnimation {targets: [exitConfirmationPopUp]; easing.type: Easing.OutBack; duration: 500}
            }
        ]
    }

    /*PopUps.StorePopUp {
        id: storePopUp
        anchors.centerIn: parent
        visible: false
        width: 600
        height: 150
    }*/
}

