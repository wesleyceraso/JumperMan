import QtQuick 2.0

Rectangle {
    id: storePopUp
    radius: 5
    color: "#FFD280"
    border {
        color: "orange"
        width: 3
    }

    signal resume
    signal exit

    Text {
        id: storeText
        text: "Store"
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: 10
        }
        font {
            pixelSize: 32
            bold: true
        }
    }

    Item {
        id: storeButtonsItem
        anchors {
            left: parent.left
            right: parent.right
            top: storeText.bottom
            bottom: parent.bottom
            margins: 20
        }
        Row {
            id: storeButtonsRow
            spacing: 20
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            CustomButton {
                height: width / 4
                width: (storeButtonsItem.width - storeButtonsRow.spacing) / 2
                text: "10 Vidas"
                upperColor: "#D6ECF3"
                bottomColor: "lightblue"
                onClicked: storePopUp.exit()
            }

            CustomButton {
                height: width / 4
                width: (storeButtonsItem.width - storeButtonsRow.spacing) / 2
                text: "100 Vidas"
                upperColor: "#D6ECF3"
                bottomColor: "lightblue"
                onClicked: storePopUp.resume()
            }
        }
    }
}

