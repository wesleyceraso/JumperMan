import QtQuick 2.0
import "../widgets"

Rectangle {
    id: gameOverPopUp
    radius: 5
    color: "#FFD280"
    border {
        color: "orange"
        width: 3
    }

    property bool hasLifes: false

    signal retry
    signal store
    signal exit

    Text {
        id: gameOverText
        text: "Oh no! You died..."
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: 10
        }
        font {
            pixelSize: gameOverPopUp.height * 0.3
            bold: true
        }
    }

    Item {
        id: gameOverButtonsItem
        anchors {
            left: parent.left
            right: parent.right
            top: gameOverText.bottom
            bottom: parent.bottom
            margins: 20
        }
        Row {
            id: gameOverButtonsRow
            spacing: 20
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            CustomButton {
                height: width / 4
                width: (gameOverButtonsItem.width - gameOverButtonsRow.spacing) / 2
                text: "Exit"
                upperColor: "#D6ECF3"
                bottomColor: "lightblue"
                onClicked: gameOverPopUp.exit()
            }

            CustomButton {
                height: width / 4
                width: (gameOverButtonsItem.width - gameOverButtonsRow.spacing) / 2
                text: hasLifes ? "Retry" : "Buy Lifes!"
                upperColor: "#D6ECF3"
                bottomColor: "lightblue"
                onClicked: hasLifes ? gameOverPopUp.retry() : gameOverPopUp.store()
            }
        }
    }
}

