import QtQuick 2.0
import "../widgets"

Rectangle {
    id: exitConfirmationPopUp
    radius: 5
    color: "#FFD280"
    border {
        color: "orange"
        width: 3
    }

    signal resume
    signal exit

    Text {
        id: gamePausedText
        text: "Confirm Exit?"
        anchors {
            horizontalCenter: parent.horizontalCenter
            top: parent.top
            topMargin: 10
        }
        font {
            pixelSize: exitConfirmationPopUp.height * 0.3
            bold: true
        }
    }

    Item {
        id: pausedButtonsItem
        anchors {
            left: parent.left
            right: parent.right
            top: gamePausedText.bottom
            bottom: parent.bottom
            margins: 20
        }
        Row {
            id: pausedButtonsRow
            spacing: 20
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            CustomButton {
                height: width / 4
                width: (pausedButtonsItem.width - pausedButtonsRow.spacing) / 2
                text: "Exit"
                upperColor: "#D6ECF3"
                bottomColor: "lightblue"
                onClicked: exitConfirmationPopUp.exit()
            }

            CustomButton {
                height: width / 4
                width: (pausedButtonsItem.width - pausedButtonsRow.spacing) / 2
                text: "Resume"
                upperColor: "#D6ECF3"
                bottomColor: "lightblue"
                onClicked: exitConfirmationPopUp.resume()
            }
        }
    }
}

