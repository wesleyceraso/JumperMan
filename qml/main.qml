import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2

import "controls" as Awesome

Window {
    id: window
    title: qsTr("JumperMan")
    visible: true
    //visibility: "FullScreen"

    /*
       640 x 1136
      1080 x 1920
       768 x 1024
       720 x 1280
       640 x 960
       480 x 800
      */
    height: 640
    width: 1136

    Component.onCompleted: {
        mainscreen.load(mainMenuComponent)
    }

    FontAwesome {
        id: awesome
        resource: "qrc:///FontAwesome.otf"
    }

    Component {
        id: mainMenuComponent
        MainMenu {}
    }

    Component {
        id: gameScreenComponent
        GameScreen {}
    }

    Loader {
        id: mainscreen
        anchors.fill: parent
        focus: true

        property variant nextSourceComponent: undefined
        property alias awesome: awesome

        Connections {
            target: mainscreen.item
            onMenu: mainscreen.load(mainMenuComponent)
            onPlay: mainscreen.load(gameScreenComponent)
        }

        function load(sourceComponent) {
            mainscreen.nextSourceComponent = sourceComponent;
            loadAnimation.start();
        }
        SequentialAnimation {
            id: loadAnimation
            NumberAnimation {target: blackCourtain; property: "opacity"; to: 1; duration: 250}
            ScriptAction { script: {
                    mainscreen.sourceComponent = mainscreen.nextSourceComponent;
                    mainscreen.nextSourceComponent = undefined;
                }
            }
            NumberAnimation {target: blackCourtain; property: "opacity"; to: 0; duration: 250}
        }
    }

    Rectangle {
        id: blackCourtain
        anchors.fill: parent
        color: "black"
    }
}
