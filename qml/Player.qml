import QtQuick 2.0
import QtMultimedia 5.0
import "platforms"

Item {
    id: root
    implicitHeight: 23
    implicitWidth: 32

    property Platform currentPlatform: null
    property int jumpTime: 250

    property int jumpToX: 0
    property int jumpToY: 0
    property Platform jumpToBar: null

    signal died
    signal landed

    function tryToMoveRight() {
        if (currentPlatform == null || currentPlatform == end)
            return;

        var nextBar = _getClosestPlatform(player, currentPlatform, gamescreen.platformColumns[currentPlatform.column + 1]);

        if (currentPlatform.release("right") && nextBar.accept(player)) {
            _jumpTo(nextBar);
        }
    }

    function tryToMoveLeft() {
        if (currentPlatform == null || currentPlatform == start)
            return;

        var nextBar = _getClosestPlatform(player, currentPlatform, gamescreen.platformColumns[currentPlatform.column - 1]);

        if (currentPlatform.release("left") && nextBar.accept(player)) {
            _jumpTo(nextBar);
        }
    }

    function _jumpTo(platform)
    {
        player.currentPlatform = null;
        player.jumpToX = platform.x + (platform.width - player.width) / 2;
        player.jumpToY = platform.yPositionInFuture(jumpTime) - player.height;
        player.jumpToBar = platform;
        jump.start();
    }

    function die() {
        jump.stop();
        sprites.jumpTo("dead");
        account.decrementLifes();
        died();
    }

    function live() {
        sprites.jumpTo("still");
    }

    function _getClosestPlatform(player, currentPlatform, targetPlatformColumn) {
        var playerPosition = player.y + player.height;
        var closestDistance = 0;
        var closestPlatform = null;

        for (var i = 0; i < targetPlatformColumn.length; ++i)
        {
            var platform = targetPlatformColumn[i];
            var distance = Math.abs(platform.yPositionInFuture(player.jumpTime / 2) - currentPlatform.yPositionInFuture(player.jumpTime / 2));
            if (closestPlatform == null || distance <= closestDistance) {
                closestDistance = distance;
                closestPlatform = platform;
            }
        }

        return closestPlatform;
    }

    SoundEffect {
        id: jumpSound
        source: "qrc:///187024__lloydevans09__jump2.wav"
    }

    SequentialAnimation {
        id: jump
        running: false
        ScriptAction {
            script: {
                jumpSound.play()
                if (player.jumpToX > player.x)
                    sprites.jumpTo("jumpingRight");
                else
                    sprites.jumpTo("jumpingLeft");
            }
        }
        ParallelAnimation {
            NumberAnimation {
                target: player
                property: "y"
                to: player.jumpToY
                easing.type: player.jumpToY < player.y ? Easing.OutBack : Easing.InBack
                easing.amplitude: 5
                duration: player.jumpTime
            }
            NumberAnimation {
                target: player
                property: "x"
                to: player.jumpToX
                easing.type: Easing.Linear
                duration: player.jumpTime
            }
        }
        ScriptAction {
            script: {
                player.currentPlatform = player.jumpToBar;
                landed();
            }
        }
    }

    SpriteSequence {
        id: sprites

        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        goalSprite: ""

        Sprite {
            name: "still"
            source: "qrc:///bob.png"
            frameCount: 1
            frameWidth: 128
            frameHeight: 92
            frameDuration: 2000
            frameX: 128 * 1
            frameY: 92
            to: {"blinking":1}
        }
        Sprite {
            name: "blinking"
            source: "qrc:///bob.png"
            frameCount: 1
            frameWidth: 128
            frameHeight: 92
            frameDuration: 100
            frameX: 128 * 0
            frameY: 92
            to: {"still":1}
        }
        Sprite {
            name: "jumpingRight"
            source: "qrc:///bob.png"
            frameDuration: jumpTime / frameCount
            frameCount: 2
            frameWidth: 128
            frameHeight: 92
            frameX: 128 * 2
            frameY: 92 * 2
            to: {"still":1}
        }
        Sprite {
            name: "jumpingLeft"
            source: "qrc:///bob.png"
            frameDuration: jumpTime / frameCount
            frameCount: 2
            frameWidth: 128
            frameHeight: 92
            frameX: 128 * 5
            frameY: 92 * 2
            to: {"still":1}
        }
        Sprite {
            name: "dead"
            source: "qrc:///bob.png"
            frameCount: 1
            frameWidth: 128
            frameHeight: 92
            frameDuration: 1000
            frameX: 128 * 0
            frameY: 0
            to: {"dead":1}
        }
    }
}

