import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtMultimedia 5.0

import "controls" as Awesome
import "panels"
import "platforms"
import "popups"
import "widgets"

Item {
    id: gamescreen

    property variant platformColumns: new Array
    property variant laserColumns: new Array
    property int platformMaximumY: 0
    property int mainLoopSleep: 50
    property Platform start: null
    property Platform end: null

    signal menu
    signal play

    state: "running"

    Component.onCompleted: {
        createLevel(20, 3);
        player.currentPlatform = start;
    }

    Connections {
        target: account
        onLifesChanged: {
            if (account.lifes <= 0)
                gamescreen.menu();
        }
    }

    Audio {
        id: gameMusic
        source: "qrc:///253378__plagasul__malletloppingnana.mp3"
        autoPlay: true
        loops: Audio.Infinite
        volume: 1
    }

    Image {anchors.fill: parent; source: "qrc:///background_level1.png"}

    Item {
        id: gamescreenSubPanel
        anchors {
            top: gamescreen.top
            bottom: gamescreen.bottom
        }

        Behavior on x { NumberAnimation { duration: 50 } }

        Player {
            id: player
            //onCurrentPlatformChanged: if (player.currentPlatform == end) levelCompleted()
            z: 1
            height: gamescreenSubPanel.height * 0.10
            width: height * 1.4

            onDied: {
                vibrator.vibrate(1000);
                gamescreen.state = "gameover"
            }

            onLanded: {
                if (player.currentPlatform.hasStar) {
                    player.currentPlatform.hasStar = false;
                    starsCounterPanel.gotStar();

                    /*var star = player.currentPlatform.cloneStar(gamescreenSubPanel);
                    var sendTo = starsPanel.nextStarCoordinates();
                    var duration = 1000 * ((sendTo.x - star.x) / sendTo.x + (star.y - sendTo.y) / gamescreenSubPanel.height);
                    var animation = gotStarAnimationComponent.createObject(gamescreenSubPanel, {"target":star, "sendTo":sendTo, "duration":duration});
                    animation.start();*/
                }
            }
        }

        layer.effect: GaussianBlur {
            source: gamescreenSubPanel
            radius: 10
            samples: radius * 2
        }
    }

    Column {
        anchors {
            top: gamescreen.top
            left: gamescreen.left
            margins: gamescreen.height * 0.01
        }

        spacing: gamescreen.height * 0.02

        TimerPanel {
            id: timerPanel
            height: gamescreen.height * 0.08
            width: height * 2.0
            running: mainLoop.running
        }

        StarsCounterPanel {
            id: starsCounterPanel
            height: gamescreen.height * 0.08
            width: height * 2.0
        }

        LifesPanel {
            id: lifesPanel
            height: gamescreen.height * 0.08
            width: height * 2.0

            lifes: account.lifes
            showTimeToGetLife: gamescreen.state != "running" && account.lifes < account.maximumLifes
        }
    }

    PausePopUp {
        id: pausePopUp
        anchors.horizontalCenter: gamescreen.horizontalCenter
        z: 1
        width: gamescreen.width * 0.70
        height: gamescreen.height * 0.30

        onResume: gamescreen.state = "running"
        onExit: gamescreen.menu()
    }

    GameOverPopup {
        id: gameOverPopUp
        anchors.horizontalCenter: gamescreen.horizontalCenter
        z: 1
        width: gamescreen.width * 0.70
        height: gamescreen.height * 0.30
        hasLifes: account.lifes > 0

        onRetry: {
            player.currentPlatform = start;
            player.live();
            starsCounterPanel.clearStarsCounter();
            timerPanel.clearTimer();
            //createLevel(6, 3);
            gamescreen.state = "running"
        }
        onExit: gamescreen.menu()
    }

    states: [
        State {
            name: "running"
            PropertyChanges {target: mainLoop; running: true}
            PropertyChanges {target: gamescreenSubPanel.layer; enabled: false}
            PropertyChanges {target: pausePopUp; visible: false}
            AnchorChanges {target: pausePopUp; anchors.top: gamescreen.bottom}
            PropertyChanges {target: gameOverPopUp; visible: false}
            AnchorChanges {target: gameOverPopUp; anchors.top: gamescreen.bottom}
        },
        State {
            name: "paused"
            PropertyChanges {target: mainLoop; running: false}
            PropertyChanges {target: gamescreenSubPanel.layer; enabled: false}
            PropertyChanges {target: pausePopUp; visible: true}
            AnchorChanges {target: pausePopUp; anchors.verticalCenter: gamescreen.verticalCenter}
            PropertyChanges {target: gameOverPopUp; visible: false}
            AnchorChanges {target: gameOverPopUp; anchors.top: gamescreen.bottom}
        },
        State {
            name: "gameover"
            PropertyChanges {target: mainLoop; running: false}
            PropertyChanges {target: gamescreenSubPanel.layer; enabled: false}
            PropertyChanges {target: pausePopUp; visible: false}
            AnchorChanges {target: pausePopUp; anchors.top: gamescreen.bottom}
            PropertyChanges {target: gameOverPopUp; visible: true}
            AnchorChanges {target: gameOverPopUp; anchors.verticalCenter: gamescreen.verticalCenter}
        }
    ]

    transitions: [
        Transition {
            from: "running"
            to: "paused"
            reversible: true
            SequentialAnimation {
                PropertyAnimation {targets: [pausePopUp, gameOverPopUp]; property: "visible"; duration: 0}
                AnchorAnimation {targets: [pausePopUp, gameOverPopUp]; easing.type: Easing.OutBack; duration: 500}
            }
        },
        Transition {
            from: "running"
            to: "gameover"
            reversible: true
            SequentialAnimation {
                PropertyAnimation {targets: [pausePopUp, gameOverPopUp]; property: "visible"; duration: 0}
                AnchorAnimation {targets: [pausePopUp, gameOverPopUp]; easing.type: Easing.OutBack; duration: 500}
            }
        }
    ]

    MouseArea {
        enabled: mainLoop.running
        anchors.fill: parent

        onClicked: {
            vibrator.vibrate(40);
            if (mouse.x < ((player.x + player.width / 2) + gamescreenSubPanel.x))
                player.tryToMoveLeft();
            else
                player.tryToMoveRight();
        }
    }

    Timer {
        id: mainLoop
        repeat: true
        interval: mainLoopSleep
        onTriggered: {
            platformColumns.forEach(function(c) { c.forEach(function(p) {
                p.y = ((gamescreen.platformMaximumY + p.y + p.height + p.velocity) % (gamescreen.platformMaximumY)) - p.height;
            })});

            if (player.currentPlatform !== null) {
                player.y = player.currentPlatform.y - player.height;
                player.x = player.currentPlatform.x + (player.currentPlatform.width - player.width) / 2;
            }
            else {
                laserColumns.forEach(function(c) {
                    if (c.length > 0) {
                        if (player.x < c[0].x && (player.x + player.width) > (c[0].x + c[0].width)) {
                            c.forEach(function (l) {
                                //It works as long as the laser beam is taller than the player
                                var diff1 = Math.abs(player.y - l.y) + player.height * 0.2;
                                var diff2 = Math.abs(player.y + player.height - (l.y + l.height)) + player.height * 0.2;
                                if (diff1 < player.height || diff2 < player.height) {
                                    player.die();
                                    return;
                                }
                            });
                            return;
                        }
                    }
                });
            }

            if ((player.y + player.height) < 0 || (player.y > gamescreen.height)) {
                player.die();
            }

            var halfWidth = gamescreen.width / 2;
            gamescreenSubPanel.x = -Math.min(Math.max((player.x + player.width / 2) - halfWidth, 0), (gamescreenSubPanel.width - gamescreen.width));
        }
    }

    focus: true
    Keys.onPressed: {
        if (state == "running") {
            switch (event.key) {
            case Qt.Key_Right:
                player.tryToMoveRight();
                break;
            case Qt.Key_Left:
                player.tryToMoveLeft();
                break;
            case Qt.Key_Back:
            case Qt.Key_P:
                gamescreen.state = "paused";
                event.accepted = true;
                break;
            default:
            }
        }
    }

    Component {
        id: leftConstrainedMovingPlatformComponent
        LeftRightConstrainedMovingPlatform {
            constraintSide: "left"
        }
    }

    Component {
        id: rightConstrainedMovingPlatformComponent
        LeftRightConstrainedMovingPlatform {
            constraintSide: "right"
        }
    }

    Component {
        id: movingPlatformComponent
        MovingPlatform {
        }
    }

    Component {
        id: laserBeamComponent
        LaserBeam {
        }
    }

    function createLaserBeam(parent, properties) {
        return laserBeamComponent.createObject(parent, properties);
    }

    function createLevel(columnCount, velocity) {
        while (gamescreen.platformColumns.length > 0) {
            var c = gamescreen.platformColumns.pop();
            while (c.length > 0) {
                c.pop().destroy();
            }
        }
        while (gamescreen.laserColumns.length > 0) {
            var c = gamescreen.laserColumns.pop();
            while (c.length > 0) {
                c.pop().destroy();
            }
        }

        var minimumColumnWidth = 100;
        var screenColumns = 8;
        var columnWidth = gamescreen.width / screenColumns;
        if (columnWidth < minimumColumnWidth) {
            screenColumns = Math.floor(columnWidth / minimumColumnWidth);
            columnWidth = gamescreen.width / screenColumns;
        }
        var totalWidth = columnCount * columnWidth;

        var platformWidth = Math.floor(columnWidth * 0.9);
        var platformHeight = Math.floor(platformWidth * 0.1);
        var platformDistance = platformWidth;
        var platformCount = Math.floor((gamescreen.height + player.height) / platformDistance);
        platformDistance = (gamescreen.height + player.height) / platformCount;

        gamescreenSubPanel.width = totalWidth;
        gamescreen.platformMaximumY = platformCount * platformDistance;

        var widthLaser = Math.floor(columnWidth * 0.1);

        for (var i = 0; i < columnCount; ++i) {
            var platformsInColumn = new Array;
            var v = (velocity) * (i % 2 ? -1 : 1);
            var x = columnWidth * i + (columnWidth - platformWidth) / 2;
            var y = 0;

            var properties = {};
            if (i == 0 || i === (columnCount - 1)) {
                y = (gamescreenSubPanel.height - platformHeight) / 2;
                var item = movingPlatformComponent.createObject(gamescreenSubPanel, {"x":x, "y":y, "velocity":0, "column":i, "width":platformWidth, "height":platformHeight});
                if (i == 0)
                    start = item;
                else
                    end = item;
                platformsInColumn.push(item);

            }
            else {
                for (var j = 0; j < platformCount; ++j) {
                    y = platformDistance * j;
                    platformsInColumn.push(movingPlatformComponent.createObject(gamescreenSubPanel, {"x":x, "y":y, "velocity":v, "column":i, "width":platformWidth, "height":platformHeight, "hasStar":(Math.random(0) > 0.5)}));
                }
            }

            var lasersInColumn = new Array;
            if (i > 1) {
                var xLaser = x - widthLaser;
                lasersInColumn.push(createLaserBeam(gamescreenSubPanel, {"x":xLaser, "y":platformDistance * 1, "width": widthLaser, "height":platformDistance, "z":1}));
                lasersInColumn.push(createLaserBeam(gamescreenSubPanel, {"x":xLaser, "y":platformDistance * 3, "width": widthLaser, "height":platformDistance, "z":1}));
            }

            platformColumns.push(platformsInColumn);
            laserColumns.push(lasersInColumn);
        }
    }

    Component {
        id: gotStarAnimationComponent
        SequentialAnimation {
            id: gotStarAnimation
            property QtObject target
            property point sendTo
            property int duration
            running: true

            ParallelAnimation {
                NumberAnimation {
                    target: gotStarAnimation.target
                    property: "x"
                    to: gotStarAnimation.sendTo.x
                    easing.type: Easing.InBack
                    duration: gotStarAnimation.duration
                }
                NumberAnimation {
                    target: gotStarAnimation.target
                    property: "y"
                    to: gotStarAnimation.sendTo.y
                    easing.type: Easing.Linear
                    duration: gotStarAnimation.duration
                }
            }
            ScriptAction {
                script: {
                    starsPanel.gotStar();
                    gotStarAnimation.target.destroy();
                    gotStarAnimation.destroy();
                }
            }
        }
    }
}

