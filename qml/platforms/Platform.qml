import QtQuick 2.0
import "../widgets"

Item {
    id: platform
    property alias hasStar: star.visible

    implicitWidth: 100
    implicitHeight: 100

    function accept(player) {
        return true;
    }
    function release(direction) {
        return true;
    }

    function yPositionInFuture(milliseconds) {
        return y;
    }

    function cloneStar(parent) {
        return starComponent.createObject(parent, {
                                            "width":star.width,
                                            "height":star.height,
                                            "x":star.x + platform.x,
                                            "y":star.y + platform.y});
    }

    Component {
        id: starComponent
        Star {}
    }

    Star {
        id: star
        anchors {
            bottom: parent.top
            bottomMargin: 10
            horizontalCenter: parent.horizontalCenter
        }
        width: parent.width / 5
        height: width
        visible: false
    }
}

