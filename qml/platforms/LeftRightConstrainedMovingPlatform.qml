import QtQuick 2.0

MovingPlatform {
    id: root

    property string constraintSide: "left"
    readonly property bool _leftConstraint: constraintSide == "left"

    function accept(player) {
        return (_leftConstraint && player.x > root.x) || (!_leftConstraint && player.x < root.x);
    }
    function release(direction) {
        return direction !== constraintSide;
    }

    Image {
        id: verticalBar
        anchors.bottom: root.top
        source: "qrc:///laser_barrier.png"

        height: 90
        width: 5

        states: [
            State {
                id: state_left
                when: _leftConstraint
                AnchorChanges {target: verticalBar; anchors.left: root.left}
            },
            State {
                id: state_right
                when: !_leftConstraint
                AnchorChanges {target: verticalBar; anchors.right: root.right}
            }
        ]
    }
}

