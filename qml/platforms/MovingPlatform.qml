import QtQuick 2.0

Platform {
    property int velocity: 0
    property int column: 0

    implicitHeight: image.implicitHeight
    implicitWidth: image.implicitWidth

    function yPositionInFuture(milliseconds) {
        var totalMoved = (milliseconds / mainLoopSleep) * velocity;
        return ((gamescreen.platformMaximumY + height + y + height + totalMoved) % (gamescreen.platformMaximumY + height)) - height;
    }

    Image {
        id: image
        source: "qrc:///platform.png"
        anchors.fill: parent
    }
}

