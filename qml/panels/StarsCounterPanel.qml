import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtMultimedia 5.0

import "../widgets" as Widgets

Rectangle {
    id: starsCounterPanel
    color: "lightblue"
    radius: height / 2

    property int counter: 0

    function gotStar() {
        collectedStartSound.play();
        counter += 1;
    }

    function clearStarsCounter() {
        counter = 0;
    }

    Widgets.Star {
        id: star
        width: starsCounterPanel.width * 0.30
        height: width

        anchors {
            verticalCenter: starsCounterPanel.verticalCenter
            verticalCenterOffset: -star.height * 0.05
            right: starsCounterPanel.horizontalCenter
        }
    }

    SoundEffect {
        id: collectedStartSound
        source: "qrc:///146723__fins__coin-object.wav"
    }

    Text {
        text: counter
        font.pixelSize: starsCounterPanel.height * 0.6
        color: "black"

        anchors {
            verticalCenter: starsCounterPanel.verticalCenter
            left: starsCounterPanel.horizontalCenter
            leftMargin: starsCounterPanel.width * 0.1
        }
    }

    layer.enabled: true
    layer.effect: DropShadow {
        radius: 4
        samples: radius * 2
        source: starsCounterPanel
        color: Qt.rgba(0, 0, 0, 0.5)
        horizontalOffset: 3
        verticalOffset: 5
        transparentBorder: true
    }
}

