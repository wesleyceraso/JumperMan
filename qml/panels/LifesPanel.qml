import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    id: lifesPanel

    property int lifes: 0
    property bool showTimeToGetLife: true
    property int _oldlifes: 0

    Behavior on lifes {
        SequentialAnimation {
            ScriptAction {
                script: {
                    lifesPanel._oldlifes = lifesPanel.lifes
                }
            }
            PropertyAnimation {}
            ScriptAction {
                script: {
                    if (lifesPanel.lifes > lifesPanel._oldlifes)
                        gotLife();
                    else
                        lostLife();
                }
            }
        }
    }

    function lostLife() {
        lostLifeAnimation.start();
    }

    function gotLife() {
        gotLifeAnimation.start();
    }

    Item {
        id: lifesSubPanel
        anchors {
            left: parent.left
            right: parent.right
        }
        height: 2 * parent.height

        Rectangle {
            id: lifesRect
            anchors {
                left: parent.left
                right: parent.right
            }
            height: lifesPanel.height
            color: "lightblue"
            radius: height / 2

            Item {
                id: heartIcon
                height: heartIconText.height
                width: heartIconText.width

                anchors {
                    verticalCenter: lifesRect.verticalCenter
                    right: lifesRect.horizontalCenter
                }

                Text {
                    id: heartIconText
                    font.family: awesome.family
                    font.pixelSize: lifesPanel.height * 0.6
                    renderType: Text.NativeRendering
                    text: awesome.icons.fa_heart
                    color: "red"
                    font.bold: true
                    visible: false
                }

                Glow {
                    id: heartLittleGlow
                    cached: true
                    radius: 1
                    spread: 0
                    samples: radius * 2
                    color: "red"
                    source: heartIconText
                    transparentBorder: true
                    anchors.fill: parent
                }

                Glow {
                    id: heartMoreGlow
                    cached: true
                    radius: 16
                    spread: 0
                    samples: radius * 2
                    color: "red"
                    source: heartIconText
                    transparentBorder: true
                    anchors.fill: parent
                    opacity: 0
                }

                Glow {
                    id: movingHeartLittleGlow
                    cached: true
                    radius: 1
                    spread: 0
                    samples: radius * 2
                    color: "red"
                    source: heartIconText
                    transparentBorder: true
                    height: parent.height
                    width: parent.width
                    opacity: 0
                }
            }

            Text {
                id: lifesText
                text: lifesPanel.lifes
                color: "black"
                font.pixelSize: lifesRect.height * 0.6
                anchors {
                    verticalCenter: lifesRect.verticalCenter
                    left: lifesRect.horizontalCenter
                    leftMargin: lifesRect.width * 0.1
                }
            }

            Rectangle {
                id: timeToGetLifeSubPanel
                color: "white"
                anchors {
                    left: parent.left
                    right: parent.right
                }
                height: parent.height
                radius: height / 2
                border {
                    color: "#D6ECF3"
                    width: 3
                }
                y: lifesPanel.showTimeToGetLife ? fakeText.contentHeight + border.width : 0
                z: -1

                Behavior on y {
                    NumberAnimation { duration: 500 }
                }

                Text {
                    id: fakeText
                    text: "##:##"
                    visible: false
                    font: timeToGetLifeText.font
                }

                Text {
                    id: timeToGetLifeText
                    font.pixelSize: lifesText.font.pixelSize * 0.8
                    anchors {
                        horizontalCenter: parent.horizontalCenter
                        bottom: parent.bottom
                        bottomMargin: 3
                    }

                    function zeroFill(number, width)
                    {
                        width -= number.toString().length;
                        if ( width > 0 )
                        {
                            return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
                        }
                        return number + "";
                    }

                    text: zeroFill(Math.floor(account.timeToGetLife / 60), 2) + ":" + zeroFill(account.timeToGetLife % 60, 2)
                }
            }
        }

        layer.enabled: true
        layer.effect: DropShadow {
            radius: 4
            samples: radius * 2
            source: lifesSubPanel
            color: Qt.rgba(0, 0, 0, 0.5)
            horizontalOffset: 3
            verticalOffset: 5
            transparentBorder: true
        }
    }

    ParallelAnimation {
        id: lostLifeAnimation
        NumberAnimation {
            target: movingHeartLittleGlow
            duration: 500
            property: "y"
            from: 0
            to: lifesSubPanel.height - movingHeartLittleGlow.height
        }
        NumberAnimation {
            target: movingHeartLittleGlow
            duration: 500
            property: "opacity"
            from: 1
            to: 0
        }
        SequentialAnimation {
            NumberAnimation {
                target: heartMoreGlow
                duration: 250
                property: "opacity"
                from: 0
                to: 1
            }
            NumberAnimation {
                target: heartMoreGlow
                duration: 250
                property: "opacity"
                from: 1
                to: 0
            }
        }
    }

    SequentialAnimation {
        id: gotLifeAnimation
        NumberAnimation {
            target: heartMoreGlow
            duration: 250
            property: "opacity"
            from: 0
            to: 1
        }
        NumberAnimation {
            target: heartMoreGlow
            duration: 250
            property: "opacity"
            from: 1
            to: 0
        }
    }
}

