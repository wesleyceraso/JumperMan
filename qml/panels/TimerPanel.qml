import QtQuick 2.0
import QtGraphicalEffects 1.0

import "../widgets" as Widgets

Rectangle {
    id: timerPanel
    color: "lightblue"
    radius: height / 2

    property alias running: timer.running
    property int elapsedSeconds: 0

    function clearTimer() {
        elapsedSeconds = 0;
    }

    Timer {
        id: timer
        interval: 1000
        running: running
        repeat: true
        onTriggered: elapsedSeconds += 1
    }

    Text {
        anchors.centerIn: timerPanel
        font.pixelSize: timerPanel.height * 0.6
        color: "black"

        function zeroFill(number, width)
        {
            width -= number.toString().length;
            if ( width > 0 )
            {
                return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
            }
            return number + "";
        }

        text: zeroFill(Math.floor(elapsedSeconds / 60), 2) + ":" + zeroFill(elapsedSeconds % 60, 2)
    }

    layer.enabled: true
    layer.effect: DropShadow {
        radius: 4
        samples: radius * 2
        source: timerPanel
        color: Qt.rgba(0, 0, 0, 0.5)
        horizontalOffset: 3
        verticalOffset: 5
        transparentBorder: true
    }
}



