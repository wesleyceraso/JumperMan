import QtQuick 2.0
import QtGraphicalEffects 1.0

import "../widgets" as Widgets

Rectangle {
    id: starsPanel
    color: "lightblue"
    radius: height / 2

    property bool gotStar1: false
    property bool gotStar2: false
    property bool gotStar3: false

    function nextStarCoordinates() {
        var point = Qt.point(starsPanel.x + starsRow.x, starsPanel.y + starsRow.y);
        if (!gotStar1) {
            point.x += star1.x;
            point.y += star1.y;
        } else if (!gotStar2) {
            point.x += star2.x;
            point.y += star2.y;
        } else {
            point.x += star3.x;
            point.y += star3.y;
        }
        return point;
    }

    function gotStar() {
        if (!gotStar1) {
            gotStar1 = true;
        } else if (!gotStar2) {
            gotStar2 = true;
        } else {
            gotStar3 = true;
        }
    }

    function clearStars() {
        gotStar1 = false;
        gotStar2 = false;
        gotStar3 = false;
    }

    Row {
        id: starsRow
        anchors {
            top: starsPanel.top
            bottom: starsPanel.bottom
            horizontalCenter: starsPanel.horizontalCenter
            margins: starsPanel.height * 0.2
        }

        spacing: starsPanel.width * 0.02

        Widgets.Star {
            id: star1
            color: gotStar1 ? "gold" : "darkgray"
            width: starsPanel.width * 0.27
            height: width
            anchors.bottom: parent.bottom
        }

        Widgets.Star {
            id: star2
            color: gotStar2 ? "gold" : "darkgray"
            width: starsPanel.width * 0.27
            height: width
            anchors.top: parent.top
        }

        Widgets.Star {
            id: star3
            color: gotStar3 ? "gold" : "darkgray"
            width: starsPanel.width * 0.27
            height: width
            anchors.bottom: parent.bottom
        }
    }

    layer.enabled: true
    layer.effect: DropShadow {
        radius: 4
        samples: radius * 2
        source: starsPanel
        color: Qt.rgba(0, 0, 0, 0.5)
        horizontalOffset: 3
        verticalOffset: 5
        transparentBorder: true
    }
}

