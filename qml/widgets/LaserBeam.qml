import QtQuick 2.0

Item {
    id: laserBeam

    implicitWidth: start.width
    implicitHeight: (start.height + end.height) * 2

    Image {
        id: start
        source: "qrc:///laser_end.png"
        anchors {
            top: laserBeam.top
            left: laserBeam.left
            right: laserBeam.right
        }
        fillMode: Image.PreserveAspectFit
    }

    Image {
        id: beam
        source: "qrc:///laser_tile.png"
        anchors {
            top: start.bottom
            bottom: end.top
            left: laserBeam.left
            right: laserBeam.right
        }
        fillMode: Image.TileVertically
    }

    Image {
        id: end
        source: "qrc:///laser_end.png"
        anchors {
            bottom: laserBeam.bottom
            left: laserBeam.left
            right: laserBeam.right
        }
        fillMode: Image.PreserveAspectFit
    }
}

