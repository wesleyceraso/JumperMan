import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import QtMultimedia 5.0

Button {
    id: button

    property color upperColor: "red"
    property color bottomColor: "blue"
    property color textColor: "black"

    onPressedChanged: {
        if (pressed) {
            buttonSound.play();
            vibrator.vibrate(40);
        }
    }

    SoundEffect {
        id: buttonSound
        source: "qrc:///213148__radiy__click.wav"
    }

    style: ButtonStyle {
        background: TwoColoredPanel {
            upperColor: !control.pressed ? control.upperColor : Qt.darker(control.upperColor)
            bottomColor: !control.pressed ? control.bottomColor : Qt.darker(control.bottomColor)
        }

        label: Text {
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: button.height * 0.6
            color: control.textColor
            text: control.text
        }
    }

    layer.enabled: !pressed
    layer.effect: DropShadow {
        radius: 4
        samples: radius * 2
        source: button
        color: Qt.rgba(0, 0, 0, 0.5)
        horizontalOffset: 3
        verticalOffset: 5
        transparentBorder: true
    }
}

