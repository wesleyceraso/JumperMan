import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    id: panel

    property color upperColor: "lightgray"
    property color bottomColor: "gray"

    Column {
        id: rawBackground
        Rectangle {
            height: panel.height / 2
            width: panel.width
            color: panel.upperColor
        }
        Rectangle {
            height: panel.height / 2
            width: panel.width
            color: panel.bottomColor
        }
        visible: false
    }

    Rectangle {
        id: mask
        anchors.fill: parent
        radius: Math.min(height / 2, width / 2)
        color: "black"
        visible: false
    }

    OpacityMask {
        anchors.fill: parent
        cached: true
        source: rawBackground
        maskSource: mask
    }
}

