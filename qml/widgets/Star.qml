import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    id: star
    property alias color: starOverlay.color

    implicitHeight: starImage.implicitHeight
    implicitWidth: starImage.implicitWidth

    Image {
        id: starImage
        anchors.fill: parent
        source: "qrc:///star.png"
        visible: false
    }

    ColorOverlay {
        id: starOverlay
        cached: true
        source: starImage
        anchors.fill: starImage
        color: "#fff200"
        visible: false
    }

    layer.enabled: true
    layer.effect: Glow {
        cached: true
        radius: 0
        source: starOverlay
        samples: radius * 2
        color: starOverlay.color
        transparentBorder: true
    }
}

