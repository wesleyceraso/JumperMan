import QtQuick 2.0

Text {
    id: faceIcon
    font.family: awesome.family
    renderType: Text.NativeRendering
    text: awesome.icons.fa_facebook_square
    color: "#3b5998"
    clip: true

    Rectangle {
        color: "white"
        anchors {
            fill: parent
            margins: 3
        }
        z: -1
    }
}

