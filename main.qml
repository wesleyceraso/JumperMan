import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2

Window {
    id: window
    title: qsTr("Hello World")
    width: 640
    height: 480
    visible: true

    Rectangle {
        anchors {
            top: parent.top
            left: parent.left
        }
        width: parent.width * 0.05
        height: parent.height * 0.40
        color: "brown"
        border {
            width: 1
            color: "black"
        }
    }

    Rectangle {
        anchors {
            top: parent.top
            right: parent.right
        }
        width: parent.width * 0.05
        height: parent.height * 0.40
        color: "brown"
        border {
            width: 1
            color: "black"
        }
    }

    Rectangle {
        anchors {
            bottom: parent.bottom
            left: parent.left
        }
        width: parent.width * 0.05
        height: parent.height * 0.40
        color: "brown"
        border {
            width: 1
            color: "black"
        }
    }

    Rectangle {
        anchors {
            bottom: parent.bottom
            right: parent.right
        }
        width: parent.width * 0.05
        height: parent.height * 0.40
        color: "brown"
        border {
            width: 1
            color: "black"
        }
    }

    Repeater {
        model: 8
        delegate: Rectangle {
            /*color: "pink"
            border {
                width: 1
                color: "black"
            }*/

            height: window.height
            width: window.width / 10
            y: 0
            x: (index + 1) * width

            property bool inverted: index % 2 != 0

            Repeater {
                model: 6
                delegate: Rectangle {
                    height: 10
                    width: parent.width * 0.90
                    anchors.horizontalCenter: parent.horizontalCenter
                    y: mystart
                    color: "black"

                    property int myindex: index
                    property double myheight: window.height / 5
                    property double mystart: myindex * myheight + (inverted ? myheight : 0) - myheight
                    property double myend: myindex * myheight + (inverted ? 0 : myheight) - myheight

                    NumberAnimation on y {loops: Animation.Infinite; duration: 3000; from: mystart; to: myend}
                }
            }
        }
    }
}
