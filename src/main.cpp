#include <QApplication>
#include <QFontDatabase>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "account.h"
#include "vibrator.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QFontDatabase::addApplicationFont(":/SF Atarian System Bold Italic.ttf");
    QFontDatabase::addApplicationFont(":/SF Atarian System Bold.ttf");
    QFontDatabase::addApplicationFont(":/SF Atarian System Extended Bold Italic.ttf");
    QFontDatabase::addApplicationFont(":/SF Atarian System Extended Bold.ttf");
    QFontDatabase::addApplicationFont(":/SF Atarian System Extended Italic.ttf");
    QFontDatabase::addApplicationFont(":/SF Atarian System Extended.ttf");
    QFontDatabase::addApplicationFont(":/SF Atarian System Italic.ttf");
    QFontDatabase::addApplicationFont(":/SF Atarian System.ttf");
    app.setFont(QFont("SF Atarian System"));

    Account account;
    Vibrator vibrator;

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("application", &app);
    engine.rootContext()->setContextProperty("account", &account);
    engine.rootContext()->setContextProperty("vibrator", &vibrator);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
