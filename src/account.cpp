#include "account.h"
#include <chrono>
#include <cmath>

using namespace std::chrono;

Account::Account(QObject *parent):
    QObject(parent),
    m_lifes(0),
    m_timeToGetLife(0),
    m_maximumLifes(20)
{
    m_lifes = m_maximumLifes;
    m_timeToGetLife = duration_cast<seconds>(seconds(20)).count();
    startTimer(duration_cast<milliseconds>(seconds(1)).count());
}

void Account::decrementLifes()
{
    m_lifes = std::max(0, m_lifes - 1);
    emit lifesChanged();
}

int Account::timeToGetLife() const
{
    return m_timeToGetLife;
}

int Account::lifes() const
{
    return m_lifes;
}

void Account::timerEvent(QTimerEvent *)
{
    if (!m_timeToGetLife--) {
        m_timeToGetLife = duration_cast<seconds>(minutes(5)).count();
        m_lifes = std::min(m_maximumLifes, m_lifes + 1);
        emit lifesChanged();
    }
    emit timeToGetLifeChanged();
}
int Account::maximumLifes() const
{
    return m_maximumLifes;
}




