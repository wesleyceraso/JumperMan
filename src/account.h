#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <QObject>

class Account : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int lifes READ lifes NOTIFY lifesChanged)
    Q_PROPERTY(int timeToGetLife READ timeToGetLife NOTIFY timeToGetLifeChanged)
    Q_PROPERTY(int maximumLifes READ maximumLifes CONSTANT)

public:
    explicit Account(QObject *parent = 0);
    Q_INVOKABLE void decrementLifes();

public slots:
    int lifes() const;
    int timeToGetLife() const;
    int maximumLifes() const;

signals:
    void lifesChanged();
    void timeToGetLifeChanged();

private slots:
    void timerEvent(QTimerEvent * event);

private:
    int m_lifes;
    int m_timeToGetLife;
    int m_maximumLifes;
};

#endif // ACCOUNT_H
